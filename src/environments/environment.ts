// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyCEhjGxEcgnvyTqz-4kjWYv_EtsXixOnII',
    authDomain: 'controle-calixto.firebaseapp.com',
    databaseURL: 'https://controle-calixto.firebaseio.com',
    projectId: 'controle-calixto',
    storageBucket: 'controle-calixto.appspot.com',
    messagingSenderId: '309687008850',
    appId: '1:309687008850:web:5ef6bf46aee4207feee98b',
    measurementId: 'G-G24NP7DZKT'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
